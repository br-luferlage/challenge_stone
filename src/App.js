import React from 'react';
import { ToastContainer } from 'react-toastify';
import { StylesProvider } from '@material-ui/core/styles';
import { Router } from 'react-router-dom';

import Routes from './routes';
import history from './services/history';

import GlobalStyle from './styles/global';

function App() {
  return (
    <Router history={history}>
      <StylesProvider injectFirst>
        <Routes />
        <GlobalStyle />
        <ToastContainer autoClose={3000} />
      </StylesProvider>
    </Router>
  );
}

export default App;
