import React from 'react';
import { Toolbar, IconButton } from '@material-ui/core';
import { MdAccountBalanceWallet } from 'react-icons/md';
import { Images } from '~/assets/img';

import {
  CustomAppBar,
  IconMenu,
  WrapperHeader,
  WrapperLogo,
  WrapperUser,
  NameUser,
  WrapperLogout,
  Logout,
} from './styles';

export default function Header() {
  return (
    <CustomAppBar>
      <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu">
          <IconMenu />
        </IconButton>
        <WrapperHeader>
          <WrapperLogo>
            <img src={Images.Logo} alt="Logo" />
          </WrapperLogo>
          <WrapperUser>
            <NameUser>Luiz Fernando</NameUser>
            <img src={Images.Separator} alt="Separador" />
            <WrapperLogout>
              <Logout>sair</Logout>
              <MdAccountBalanceWallet color="#C3C3C3" size={20} />
            </WrapperLogout>
          </WrapperUser>
        </WrapperHeader>
      </Toolbar>
    </CustomAppBar>
  );
}
