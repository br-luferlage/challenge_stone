import styled from 'styled-components';
import { AppBar, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

export const CustomAppBar = styled(AppBar).attrs({
  position: 'static',
})`
  background: #fff;
  justify-content: center;
`;

export const IconMenu = styled(MenuIcon)`
  color: #000;
`;

export const WrapperHeader = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

export const WrapperLogo = styled.div`
  img {
    width: 90px;
    height: 52px;
  }
`;

export const WrapperUser = styled.div`
  display: flex;
  align-items: center;
`;

export const NameUser = styled(Typography).attrs({
  variant: 'body2',
})`
  color: #989696;
  margin-right: 10px;
`;

export const WrapperLogout = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Logout = styled(Typography).attrs({
  variant: 'body2',
})`
  color: #a3aaa3;
  margin-right: 5px;
  margin-left: 10px;
`;
