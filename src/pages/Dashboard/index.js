import React from 'react';
import { Paper } from '@material-ui/core';

import { Container, Quotes, Extract, CustomDivider } from './styles';

export default function Dashboard() {
  return (
    <Container>
      <Quotes xl={3} xs={12}>
        <h1>Cotacoes</h1>
        <CustomDivider />
        <Paper>COTACOES</Paper>
      </Quotes>
      <Extract item xl={9} xs={12}>
        <Paper>CARTEIRA</Paper>
        <Paper>EXTRATO</Paper>
      </Extract>
    </Container>
  );
}
