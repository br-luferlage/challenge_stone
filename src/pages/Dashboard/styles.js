import styled from 'styled-components';
import { Grid, Divider } from '@material-ui/core';

export const Container = styled(Grid).attrs({
  container: true,
  spacing: 3,
})``;

export const Quotes = styled(Grid).attrs({
  item: true,
})`
  h1 {
    font-size: 40px;
    color: #f00;
    text-decoration: underline;
  }
`;

export const Extract = styled(Grid).attrs({
  item: true,
})``;

export const CustomDivider = styled(Divider)``;
