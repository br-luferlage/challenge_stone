import React, { useState } from 'react';
import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import history from '~/services/history';
import { Images } from '~/assets/img';

const schema = Yup.object().shape({
  email: Yup.string()
    .email('*Informe um e-mail válido')
    .required('*e-mail é obrigatório'),
  password: Yup.string().required('*senha é obrigatória'),
});

export default function SignIn() {
  const [email] = useState('luizfonseca@stone.com.br');
  const [password] = useState('contratado!');

  function handleSubmit(data) {
    // Verifica se dados do usuario conferem com o previsto no state!
    if (data.email === email && data.password === password) {
      const user = {
        email: data.email,
        password: data.password,
      };

      // Salva user no localStorage
      localStorage.setItem('@user', JSON.stringify(user));

      // Encaminha o user para a rota
      history.push('/dashboard');
    } else {
      // Incluir um Toast notificando que os dados estão inválidos
      toast.error('Falha ao autenticar, verifique seus dados');
    }
  }

  return (
    <>
      <img src={Images.Logo} alt="StoneCo" />
      <Form schema={schema} onSubmit={handleSubmit}>
        <Input name="email" placeholder="e-mail" />
        <Input name="password" type="password" placeholder="senha" />

        <button type="submit">Entrar</button>
      </Form>
    </>
  );
}
