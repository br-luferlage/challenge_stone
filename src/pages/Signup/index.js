import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';

import { Images } from '~/assets/img';

const schema = Yup.object().shape({
  fullname: Yup.string().required('nome completo é obrigatório'),
  email: Yup.string()
    .email('*Informe um e-mail válido')
    .required('*e-mail é obrigatório'),
  password: Yup.string().required('*senha é obrigatória'),
  confirm_password: Yup.string().required(
    '*confirmação de senha é obrigatório'
  ),
});

export default function SignUp() {
  function handleSubmit() {}

  return (
    <>
      <img src={Images.Logo} alt="StoneCo" />
      <Form schema={schema} onSubmit={handleSubmit}>
        <Input name="fullname" placeholder="nome completo" />
        <Input name="email" placeholder="e-mail" />
        <Input name="password" type="password" placeholder="senha" />
        <Input
          name="confirm_password"
          type="password"
          placeholder="confirme sua senha"
        />

        <button type="submit">Criar conta</button>
        <Link to="/">
          já tenho <span>login</span>
        </Link>
      </Form>
    </>
  );
}
